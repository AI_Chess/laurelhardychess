var sleep = 250;

var startFEN = '4k3/8/8/8/8/8/8/3RK3 w - - 0 1';

// pass in a FEN string to load a particular position
var game = new Chess();

if (game.load(startFEN)==false)
{
  alert('Invalid FEN');
}

board = ChessBoard('board', game.fen());

var makeMove = function () {

  if (game.turn() == 'b'){
    window.setTimeout(makeBlackMove, sleep);
  }
  else{
    window.setTimeout(makeWhiteMove, sleep);
  }
  if (game.game_over() === true ||
  game.in_draw() === true) return;
  
  window.setTimeout(makeMove, sleep);
}

var makeWhiteMove = function() {

  var possibleMoves = game.moves();

  // exit if the game is over
  if (game.game_over() === true ||
    game.in_draw() === true ||
    possibleMoves.length === 0) return;

  var randomIndex = Math.floor(0);
  game.move(possibleMoves[randomIndex]);
  board.position(game.fen());

  return;
};

var makeBlackMove = function() {
  var possibleMoves = game.moves();

  // exit if the game is over
  if (game.game_over() === true ||
    game.in_draw() === true ||
    possibleMoves.length === 0) return;

  var randomIndex = Math.floor(Math.random() * possibleMoves.length);
  game.move(possibleMoves[randomIndex]);
  board.position(game.fen());

  return;
};

window.setTimeout(makeMove, 500);